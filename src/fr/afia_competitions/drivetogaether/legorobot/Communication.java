package fr.afia_competitions.drivetogaether.legorobot;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.Queue;
import java.util.EmptyQueueException;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

public class Communication extends Thread {
    private NXTConnection connection = null;
    private Queue<Instruction> instructions = new Queue<>();
    private String controllerName = "";

    public Communication() {
        super();
        DataOutputStream outStream;
        BufferedReader inputBuffer;

        // TODO: allow to restart the program while waiting for a connection
        this.connection = Bluetooth.waitForConnection();

        try {
            outStream = this.connection.openDataOutputStream();
            outStream.writeChars ( Bluetooth.getFriendlyName() );
            outStream.flush();
            outStream.close();
        } catch ( IOException e ) {
            e.printStackTrace();
            Button.waitForAnyPress();
        }

        try {
            inputBuffer = new BufferedReader ( new InputStreamReader ( this.connection.openInputStream(), "UTF-8" ) );
            this.controllerName = inputBuffer.readLine();
        } catch ( IOException e ) {
            e.printStackTrace();
            Button.waitForAnyPress();
        }
    }

    public void run() {
        Instruction instruction;
        BufferedReader inputBuffer;

        try {
            inputBuffer = new BufferedReader ( new InputStreamReader ( this.connection.openInputStream(), "UTF-8" ) );
            while ( true ) {
                // Get instructions
                instruction = new Instruction ( inputBuffer.readLine() );
                synchronized ( instructions ) {
                    instructions.push ( instruction );
                }
            }
        } catch ( Exception e ) {
            e.printStackTrace();
            Button.waitForAnyPress();
        }
    }

    public Instruction nextInstruction() throws EmptyQueueException {
        Instruction instruction;
        synchronized ( this.instructions ) {
            instruction = ( Instruction ) this.instructions.pop();
        }
        return instruction;
    }

    public void send ( Instruction toSend ) {
        this.send(toSend.toString());
    }

    public void send ( String toSend ) {
        DataOutputStream outStream;
        try {
            outStream = this.connection.openDataOutputStream();
            outStream.writeChars ( toSend );
            outStream.flush();
            outStream.close();
        } catch ( IOException e ) {
            e.printStackTrace();
            Button.waitForAnyPress();
        }
    }

    public String getControllerName() {
        return controllerName;
    }
}
