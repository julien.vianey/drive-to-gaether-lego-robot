package fr.afia_competitions.drivetogaether.legorobot;

import java.util.EmptyQueueException;
import java.util.List;

import lejos.nxt.LCD;
import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.Motor;
import lejos.nxt.LightSensor;
import lejos.nxt.Sound;
import lejos.nxt.comm.Bluetooth;
import lejos.util.Delay;


public class LineFollower {
    private static final LightSensor LEFT_SENSOR = new LightSensor ( SensorPort.S2 );
    private static final LightSensor RIGHT_SENSOR = new LightSensor ( SensorPort.S1 );
    private static final NXTRegulatedMotor LEFT_MOTOR = Motor.C;
    private static final NXTRegulatedMotor RIGHT_MOTOR = Motor.B;
    private static final int SENSOR_VALUE_TARGET = 450;
    private static final double MOTOR_MEAN_SPEED = .2 * LEFT_MOTOR.getMaxSpeed(); // I consider the max speed to be similar for both motors
    private static final int MIN_TIME_TO_CROSS_INTERSECTION = 5000; // milli seconds

    public static void main ( String[] args ) throws Exception {
        Instruction instruction;
        Instruction whatHasBeenDone;

        RIGHT_SENSOR.setHigh ( 100 );
        LEFT_SENSOR.setHigh ( 100 );

        LCD.drawString ( Bluetooth.getFriendlyName() + "\n" + Bluetooth.getLocalAddress() + "\n\nWaiting for\nconnection", 0, 0 );
        Communication comm = new Communication();

        LCD.clear();
        LCD.drawString ( "Connected to \n" + comm.getControllerName() + "\n\nPress any button\nto start", 0, 0 );
        Button.waitForAnyPress();
        comm.setDaemon ( true );
        comm.start();
        LCD.clear();

        while ( Button.ESCAPE.isUp() ) {
            try {
                instruction = comm.nextInstruction();
            } catch ( EmptyQueueException e ) {
                LCD.clear();
                LCD.drawString ( "Waiting for\ninstructions", 0, 0 );
                Delay.msDelay ( 50 );
                continue;
            }
            whatHasBeenDone = new Instruction ( "" );
            LCD.clear();
            LCD.drawString ( "Instruction:\n" + instruction.toString(), 0, 0 );

            if ( instruction.hasDirection() ) {
                RIGHT_MOTOR.forward();
                LEFT_MOTOR.forward();
                switch ( instruction.getDirection() ) {
                case STRAIGHT:
                    crossTheLine();
                    followLine();
                    whatHasBeenDone.setDirection ( Instruction.Direction.STRAIGHT );
                    break;
                case UTURN:
                    uTurn();
                    whatHasBeenDone.setDirection ( Instruction.Direction.UTURN );
                    break;
                case LEFT:
                    crossTheLine();
                    followLeftLine();
                    whatHasBeenDone.setDirection ( Instruction.Direction.LEFT );
                    break;
                case RIGHT:
                    crossTheLine();
                    followRightLine();
                    whatHasBeenDone.setDirection ( Instruction.Direction.RIGHT );
                    break;
                }
                LEFT_MOTOR.stop();
                RIGHT_MOTOR.stop();
            }

            if ( instruction.hasActions() ) {
                List<Instruction.Action> actions = instruction.getActions();
                for ( int i = 0, n = actions.size(); i < n; i++ ) {
                    switch ( actions.get ( i ) ) {
                    case TAKE_VICTIM:
                        takeVictim();
                        whatHasBeenDone.addAction ( Instruction.Action.TAKE_VICTIM );
                        break;
                    case DROP_VICTIM:
                        dropVictim();
                        whatHasBeenDone.addAction ( Instruction.Action.DROP_VICTIM );
                        break;
                    }
                }
            }

            comm.send ( whatHasBeenDone );
        }
    }

    private static Boolean line ( LightSensor sensor ) {
        int nb_white = 0;

        for ( int i = 0; i < 9; i++ ) {
            if ( sensor.getLightValue() > SENSOR_VALUE_TARGET ) {
                nb_white++;
            }
            Delay.msDelay(5);
        }

        // nb_white ranges from 0 to 9, so the values to 9 represents the upper half of the possible values
        return nb_white > 5;
    }

    private static Boolean onCrossingLine () {
        return line ( LEFT_SENSOR ) && line ( RIGHT_SENSOR );
    }

    private static void crossTheLine() {
        RIGHT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        LEFT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );

        while ( onCrossingLine() );
    }

    private static void followLine() {
        RIGHT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        LEFT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );

        while ( ! onCrossingLine() ) {
            regulateMotor ( LEFT_SENSOR, LEFT_MOTOR );
            regulateMotor ( RIGHT_SENSOR, RIGHT_MOTOR );

        }
    }

    private static void uTurn() {
        Boolean lineSeen = false, backOnBlack = false;
        RIGHT_MOTOR.backward();
        LEFT_MOTOR.backward();

        crossTheLine();
        Delay.msDelay(500);
        RIGHT_MOTOR.forward();

        RIGHT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        LEFT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        while ( !line ( LEFT_SENSOR ));
        Delay.msDelay(50);
        LEFT_MOTOR.forward();
    }

    private static void followLeftLine() {
        long start = System.currentTimeMillis();

        RIGHT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        LEFT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );

        while ( ! onCrossingLine()
                ||System.currentTimeMillis() - start < MIN_TIME_TO_CROSS_INTERSECTION ) {
            regulateMotor ( LEFT_SENSOR, LEFT_MOTOR );
        }
    }

    private static void followRightLine() {
        long start = System.currentTimeMillis();

        RIGHT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );
        LEFT_MOTOR.setSpeed ( ( int ) MOTOR_MEAN_SPEED );

        while ( ! onCrossingLine()
                || System.currentTimeMillis() - start < MIN_TIME_TO_CROSS_INTERSECTION ) {
            regulateMotor ( RIGHT_SENSOR, RIGHT_MOTOR );
        }
    }

    private static void takeVictim() {
        Sound.playTone ( 1400, 100 );
        Sound.pause ( 100 );
        Sound.pause ( 100 );
        Sound.playTone ( 2100, 100 );
        Sound.pause ( 100 );
    }

    private static void dropVictim() {
        Sound.twoBeeps();
    }

    private static void regulateMotor ( LightSensor sensor, NXTRegulatedMotor motor ) {
        int sensorMaxDist = 30;
        double motorMaxDist = .2 * motor.getMaxSpeed();
        int sensorValue = sensor.getLightValue();
        int motorSpeedChange = ( int ) ( ( -1 * motorMaxDist ) / sensorMaxDist );
        int motorSpeedAtZero = ( int ) ( MOTOR_MEAN_SPEED - motorSpeedChange * SENSOR_VALUE_TARGET );

        if ( sensorValue < SENSOR_VALUE_TARGET - sensorMaxDist ) {
            motor.setSpeed ( ( int ) ( MOTOR_MEAN_SPEED + motorMaxDist ) );
        } else if ( sensorValue > SENSOR_VALUE_TARGET + sensorMaxDist ) {
            motor.setSpeed ( 1 );
        } else {
            motor.setSpeed ( motorSpeedChange * sensorValue + motorSpeedAtZero );
        }
    }
}
